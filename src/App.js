import React, { Component } from 'react';
import './App.css';

class Square extends Component {
    constructor(props){
        super(props);
        this.state = {
            isToggleOn: props.isToggleOn,
            x: props.x,
            y: props.y
        };
        this.handleClick = this.handleClick.bind(this)
    }
    handleClick() {
        this.setState(prevState => ({
            isToggleOn: !prevState.isToggleOn
        }));
    }
    render() {
        return (
                <button
                    className={this.state.isToggleOn ? "square selected" :"square"}
                    onClick={this.handleClick}>
                    {"" + this.state.x + this.state.y}
                </button>)
    }
}

function Repeat(props) {
    let items = [];
    for (let i = 0; i < props.numTimes; i++) {
        items.push(props.children(i));
    }
    return <div>{items}</div>;
}

class BoardRow extends Component {
    render() {
        return (
                <div className="board-row">
                    <Repeat numTimes={this.props.row.length}>
                        {(index) => <Square  key={index} y={index} x={this.props.x} isToggleOn={this.props.row[index]}></Square>}
                    </Repeat>
                </div>
        )
    }
}

class App extends Component {
    constructor(props) {
        super(props);

        let board = [];
        for(let i=0; i<10; i++) {
            let row = [];
            for(let j=0; j<10; j++) {
                row.push(false);
            }
            board.push(row);
        }

        this.state = {
            board: board
        }
    }
  render() {
    return (
            <div className="game">
                <div className="board">
                    <Repeat numTimes={this.state.board.length}>
                        {(index) => <BoardRow  key={index} x={index} row={this.state.board[index]}></BoardRow>}
                    </Repeat>
                </div>
            </div>);
  }
}

export default App;
